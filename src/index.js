import React from 'react';
// import 'bootstrap/dist/css/dashboard.css';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css';
import 'datatables.net-dt/css/jquery.dataTables.css';
import 'datatables.net-dt/css/jquery.dataTables.min.css';
import ReactDOM from "react-dom";
import { BrowserRouter, Routes, Route} from "react-router-dom";
// import App from './App';
import Header from './components/header';
import Login from './components/login';
import Student from './components/master/student';
import Lecturer from './components/master/lecturer';
import Course from './components/master/course';
import UnivLocation from './components/master/univlocation'

export default function App() {
  return (
    <BrowserRouter>
      <Header/>
      <Routes>
          <Route path="login" element={<Login/>}/>
          <Route path="student" element={<Student/>}/>
          <Route path="lecturer" element={<Lecturer/>}/>
          <Route path="course" element={<Course/>}/>
          <Route path="univlocation" element={<UnivLocation/>}/>
      </Routes>
    </BrowserRouter>
  )
}

ReactDOM.render(<App />, document.getElementById("root"));