import React from 'react';
import { Link } from "react-router-dom";
import { FormGroup, Label } from 'reactstrap';

  const Login = () => {
    return (
        // <Outlet>
        <main className="container">
            <h1 className="text-white text-uppercase text-center my-4">Login Attendance App</h1>
            <div className="row">
                <div className="col-md-6 col-sm-10 mx-auto p-0">
                    <div className="card p-3">
                        <FormGroup>
                        <input type="text" name="username" id="username" placeholder='Username' />
                        </FormGroup>
                        <FormGroup>
                        <input type="password" name="password" id="password" placeholder='Password' />
                        </FormGroup>
                        <FormGroup>
                        <div className="mb-4">
                            <Link to="/student"> <button className="btn btn-primary">Login</button> </Link>
                            <Label check><input type="checkbox" name="saveAuth" id="saveAuth"/>Remember Me !</Label>
                        </div>
                        </FormGroup>
                    </div>
                </div>
            </div>
        </main> 
    //   </Outlet>
    );
}

export default Login;

