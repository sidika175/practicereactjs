import React from "react";
// import 'bootstrap/dist/css/dashboard.css';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'feather-icons/dist/feather';
import 'feather-icons/dist/feather.min';

export default class SideBar extends React.Component {

  render(){

   return(

    <nav className="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
      <div className="sidebar-sticky pt-3">
        <ul className="nav flex-column">
          <li className="nav-item">
            <a className="nav-link active" href="/#">
              <span data-feather="home"></span>
              Dashboard <span className="sr-only"></span>
            </a>
          </li>
          <li className="nav-item">
            <a className="nav-link" href="/student">
              <span data-feather="file"></span>
              Students
            </a>
          </li>
          <li className="nav-item">
            <a className="nav-link" href="/lecturer">
              <span data-feather="shopping-cart"></span>
              Lecturers
            </a>
          </li>
          <li className="nav-item">
            <a className="nav-link" href="/course">
              <span data-feather="users"></span>
              Courses
            </a>
          </li>
          <li className="nav-item">
            <a className="nav-link" href="/schedule">
              <span data-feather="bar-chart-2"></span>
              Schedule
            </a>
          </li>
          <li className="nav-item">
            <a className="nav-link" href="/univlocation">
              <span data-feather="bar-chart-2"></span>
              Univ Location
            </a>
          </li>
        </ul>
      </div>
    </nav>
   )
  }
}
