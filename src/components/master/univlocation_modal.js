import React, {Component} from "react";
import {
    Button,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    Form,
    FormGroup,
    Input,
    Label,
  } from "reactstrap";

  export default class CourseModal extends Component{
    constructor(props){
        super(props);
        this.state = {
            activeItem : this.props.activeItem,
        };
    }

    handleChange = (e) => {
        let { name, value } = e.target;
    
        const activeItem = { ...this.state.activeItem, [name]: value };
    
        this.setState({ activeItem });
    };

  render() {
    console.log(this.state);
    const { toggle, onSave } = this.props;
    return (
      <Modal isOpen={true} toggle={toggle}>
        <ModalHeader toggle={toggle}></ModalHeader>
        <ModalBody>
          <Form>
            <FormGroup>
              <Label for="univId">University Id</Label>
              <Input
                type="text"
                id="univId"
                name="univId"
                value={this.state.activeItem.univId}
                onChange={this.handleChange}
                placeholder="University Id"
                maxLength={5}
              />
            </FormGroup>
            <FormGroup>
              <Label for="name">Name</Label>
              <Input
                type="text"
                id="name"
                name="name"
                value={this.state.activeItem.name}
                onChange={this.handleChange}
                placeholder="Input Name"
              />
            </FormGroup>
            <FormGroup>
              <Label for="latitude">Latitude</Label>
              <Input
                type="text"
                id="latitude"
                name="latitude"
                value={this.state.activeItem.latitude}
                onChange={this.handleChange}
                placeholder="Input Latitude"
              />
            </FormGroup>
            <FormGroup>
              <Label for="longitude">Longitude</Label>
              <Input
                type="text"
                id="longitude"
                name="longitude"
                value={this.state.activeItem.longitude}
                onChange={this.handleChange}
                placeholder="Input Longitude"
              />
            </FormGroup>
          </Form>
        </ModalBody>
        <ModalFooter>
          <Button
            color="success"
            onClick={() => onSave(this.state.activeItem)}
          >
            Save
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
}