import React, {Component} from "react";
import {
    Button,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    Form,
    FormGroup,
    Input,
    Label,
  } from "reactstrap";

  export default class CourseModal extends Component{
    constructor(props){
        super(props);
        this.state = {
            activeItem : this.props.activeItem,
        };
    }

    handleChange = (e) => {
        let { name, value } = e.target;
    
        const activeItem = { ...this.state.activeItem, [name]: value };
    
        this.setState({ activeItem });
    };

  render() {
    console.log(this.state);
    const { toggle, onSave } = this.props;
    return (
      <Modal isOpen={true} toggle={toggle}>
        <ModalHeader toggle={toggle}></ModalHeader>
        <ModalBody>
          <Form>
            <FormGroup>
              <Label for="nikL">NIK</Label>
              <Input
                type="text"
                id="nikL"
                name="nikL"
                value={this.state.activeItem.nikL}
                onChange={this.handleChange}
                placeholder="Input NIK"
                maxLength={16}
              />
            </FormGroup>
            <FormGroup>
              <Label for="name">Name</Label>
              <Input
                type="text"
                id="name"
                name="name"
                value={this.state.activeItem.name}
                onChange={this.handleChange}
                placeholder="Input Name"
              />
            </FormGroup>
            <FormGroup>
              <Label for="Gender">gender</Label>
              <Input
                type="text"
                id="gender"
                name="gender"
                value={this.state.activeItem.gender}
                onChange={this.handleChange}
                placeholder="Input Gender"
              />
            </FormGroup>
            <FormGroup>
              <Label for="address">Address</Label>
              <Input
                type="text"
                id="address"
                name="address"
                value={this.state.activeItem.address}
                onChange={this.handleChange}
                placeholder="Input Address"
              />
            </FormGroup>
          </Form>
        </ModalBody>
        <ModalFooter>
          <Button
            color="success"
            onClick={() => onSave(this.state.activeItem)}
          >
            Save
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
}