import React, { Component } from 'react';
import axios from "axios";
import Modal from './course_modal'
import SideBar from '../sidebar';
import $ from 'jquery';
import 'datatables.net';

export class Course extends Component{
    constructor(props){
      super(props);
    //   this.editItem= this.editItem.bind(this);
      this.state = {
        CourseList : [],
        modal: false,
        loading: true,
        activeItem: {
            nikl : "",
            name : "",
            gender : "",
            address: "",
          },
      }
    };

    refreshTable(){
        $('#courseTable').DataTable().clear().destroy();
        this.componentDidMount();
    }

    async getCoursesData(){
        const res = await axios.get("/api/courses/");
        console.log(res.data);
        this.setState({loading: false, courseList: res.data});
    }

    componentDidMount() {
        this.getCoursesData().then(() => this.syncDataTable());
    }
    
    syncDataTable(){
        var editor;
        
        editor = $('#courseTable').DataTable({
            data: this.state.courseList,
            columns: [
                {title: 'Code',data: 'code'},
                {title: 'Name',data: 'name'},
                {title: 'SKS',data: 'sks'},
                {title: 'Description',data: 'desc'},
                {title: 'Created Date',data: 'created_at'},
                {
                    data: null,
                    orderable: false,
                    targets: -1,
                    defaultContent: '<button type="button" className="btn btn-warning">Update</button>',
                    createdCell: (td, cellData, rowData, row, col) => {
                        $(td).click(e => {
                            console.log(rowData)
                            this.editItem(rowData);
                            
                        })},
                },
                {
                    data: null,
                    orderable: false,
                    defaultContent: '<button type="button" className="btn btn-danger">Delete</button>',
                    createdCell: (td, cellData, rowData, row, col) => {
                        $(td).click(e => {
                            console.log(rowData)
                            this.handleDelete(rowData);
                        })},
                },
            ]            
        });
        
        $('#courseTable').on('click', 'td.editor-delete', function (e) {
            e.preventDefault();
            console.log("deleted")
        } );
    }

    toggle = () => {
        this.setState({ modal: !this.state.modal });
    };
    
    handleSubmit = (item) => {
        this.toggle();
        if (item.id) {
            axios
                .put(`/api/courses/${item.id}/`, item)
                .then((res) => this.refreshTable());
            return;
        }
        axios
            .post("/api/courses/", item)
            .then((res) => this.refreshTable());
    };

    handleDelete = (item) => {
        if (item.id) {
            axios
                .delete(`/api/courses/${item.id}/`, item)
                .then((res) => this.refreshTable());
            return;
        }
    };

    createItem = () => {
        const item = { 
            nim: "", 
            name: "", 
            gender: "",
            address: "",
        };
        this.setState({ activeItem : item, modal: !this.state.modal });
        console.log(this.state);
    };
    
    editItem = (item) => {
        console.log("edited")
        this.setState({ activeItem: item, modal: !this.state.modal });
    };
      
    render(){
        return (
            // <>
            <div className="container-fluid">
                <div className="row">
                    <SideBar/>
                    <main role="main" className="col-md-9 ml-sm-auto col-lg-10 px-md-4">
                        <div className="row">
                            <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                                <div className="grid">
                                    <div className="row">
                                        <h2>Courses</h2>
                                    </div> 
                                    <div className="row">
                                        <button className="btn btn-sm btn-primary" onClick={this.createItem}>Add +</button>
                                    </div>
                                </div>
                            </div> 
                            <div className="table-responsive">
                                <table className="table table-striped table-bordered"
                                    id="courseTable" name="courseTable" width="100%">
                                </table>
                            </div>
                        </div>
                     </main>
                 </div>
                 {this.state.modal ? (
                    <Modal
                        activeItem={this.state.activeItem}
                        toggle={this.toggle}
                        onSave={this.handleSubmit} />
                ) : null}
            </div>
        );
    }
}

export default Course;
