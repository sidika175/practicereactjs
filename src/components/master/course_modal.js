import React, {Component} from "react";
import {
    Button,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    Form,
    FormGroup,
    Input,
    Label,
  } from "reactstrap";

  export default class CourseModal extends Component{
    constructor(props){
        super(props);
        this.state = {
            activeItem : this.props.activeItem,
        };
    }

    handleChange = (e) => {
        let { name, value } = e.target;
    
        const activeItem = { ...this.state.activeItem, [name]: value };
    
        this.setState({ activeItem });
    };

  render() {
    console.log(this.state);
    const { toggle, onSave } = this.props;
    return (
      <Modal isOpen={true} toggle={toggle}>
        <ModalHeader toggle={toggle}>Course</ModalHeader>
        <ModalBody>
          <Form>
            <FormGroup>
              <Label for="code">Code</Label>
              <Input
                type="text"
                id="code"
                name="code"
                value={this.state.activeItem.code}
                onChange={this.handleChange}
                placeholder="Input Code"
                maxLength={16}
              />
            </FormGroup>
            <FormGroup>
              <Label for="name">Name</Label>
              <Input
                type="text"
                id="name"
                name="name"
                value={this.state.activeItem.name}
                onChange={this.handleChange}
                placeholder="Input Name"
              />
            </FormGroup>
            <FormGroup>
              <Label for="Gender">SKS</Label>
              <Input
                type="text"
                id="sks"
                name="sks"
                value={this.state.activeItem.sks}
                onChange={this.handleChange}
                placeholder="Input SKS"
              />
            </FormGroup>
            <FormGroup>
              <Label for="desc">Description</Label>
              <Input
                type="text"
                id="desc"
                name="desc"
                value={this.state.activeItem.desc}
                onChange={this.handleChange}
                placeholder="Input Description"
              />
            </FormGroup>
          </Form>
        </ModalBody>
        <ModalFooter>
          <Button
            color="success"
            onClick={() => onSave(this.state.activeItem)}
          >
            Save
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
}